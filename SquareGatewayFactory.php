<?php
namespace Payum\Square;

use Payum\Square\Action\AuthorizeAction;
use Payum\Square\Action\CancelAction;
use Payum\Square\Action\ConvertPaymentAction;
use Payum\Square\Action\CaptureAction;
use Payum\Square\Action\NotifyAction;
use Payum\Square\Action\RefundAction;
use Payum\Square\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

class SquareGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults([
            'payum.factory_name' => 'square',
            'payum.factory_title' => 'Square',
            'payum.action.capture' => new CaptureAction(),
            'payum.action.authorize' => new AuthorizeAction(),
            'payum.action.refund' => new RefundAction(),
            'payum.action.cancel' => new CancelAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        //access_token,location_id

        if (false == $config['payum.api']) {
            $config['payum.default_options'] = [
                'access_token' => '',
                'location_id' => ''
            ];
            $config->defaults($config['payum.default_options']);
            $config['payum.required_options'] = ['access_token', 'location_id'];

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);
                return new Api([
                    'access_token' => $config['access_token'],
                    'location_id' => $config['location_id'],
                ],
                    $config['payum.http_client'],
                    $config['httplug.message_factory']);
            };
        }
    }
}
