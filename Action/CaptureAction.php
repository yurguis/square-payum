<?php
namespace Payum\Square\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\LogicException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Square\Api;
use SquareConnect\Api\TransactionsApi;
use SquareConnect\ApiException;
use SquareConnect\Configuration;
use SquareConnect\Model\ChargeRequest;
use SquareConnect\Model\Tender;

class CaptureAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    public function __construct()
    {
        $this->apiClass = Api::class;
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        if ($model['status']) {
            return;
        }

        if (false == ($model['card_nonce'] || $model['customer_id'])) {
            throw new LogicException('The either card token or customer id has to be set.');
        }

        try {
            $config = $this->api->getOptions();
            Configuration::getDefaultConfiguration()->setAccessToken($config['access_token']);
            $transactionsApi = new TransactionsApi();
            $chargeRequest = new ChargeRequest($model->toUnsafeArrayWithoutLocal());
            $chargeResponse = $transactionsApi->charge($config['location_id'], $chargeRequest);
            $transaction = $chargeResponse->getTransaction();
            $transactionTenders = $transaction->getTenders()[0];
            $model['id'] = $transaction->getId();
            $model['location_id'] = $transaction->getLocationId();
            $model['created_at'] = $transaction->getCreatedAt();
            $model['tenders'] = [
                "id" => $transactionTenders->getId(),
                "location_id" => $transactionTenders->getLocationId(),
                "transaction_id" => $transactionTenders->getTransactionId(),
                "created_at" => $transactionTenders->getCreatedAt(),
                "note" => $transactionTenders->getNote(),
                "amount_money" => [
                    "amount" => $transactionTenders->getAmountMoney()->getAmount(),
                    "currency" => $transactionTenders->getAmountMoney()->getCurrency()
                ],
                "type" => $transactionTenders->getType(),
                "card_details" => [
                    "status" => $transactionTenders->getCardDetails()->getStatus(),
                    "card" => [
                        "card_brand" => $transactionTenders->getCardDetails()->getCard()->getCardBrand(),
                        "last_4" => $transactionTenders->getCardDetails()->getCard()->getLast4(),
                        "fingerprint" => $transactionTenders->getCardDetails()->getCard()->getFingerprint()
                    ],
                    "entry_method" => $transactionTenders->getCardDetails()->getEntryMethod()
                ]
            ];
            $model['product'] = $transaction->getProduct();

            unset($model['card_nonce']);
            unset($model['idempotency_key']);
            unset($model['amount_money']);
        } catch (ApiException $e) {
            $model->replace(get_object_vars($e->getResponseBody()));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess
            ;
    }
}
